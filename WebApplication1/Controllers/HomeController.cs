﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        private AppContext db = new AppContext();

        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Uczestnicy()
        {
            return View(db.Uczestnicy);
        }

        public ActionResult A()
        {
            return View();
        }

        public String String()
        {
            return "Wszystkiego dobrego!";
        }
        [HttpGet]
        public ViewResult Formularz()
        {
            return View();
        }
        [HttpPost]
        public ViewResult Formularz(Uczestnik u)
        {
            if (ModelState.IsValid)
            {
                if(u.CzyUczestniczy==true)
                {
                    db.Uczestnicy.Add(u);
                    db.SaveChanges();
                }
                return View("Dziękuje", u);
            }
            else
                return View();

        }
    }
}