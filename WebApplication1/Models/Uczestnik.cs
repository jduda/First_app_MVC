﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class Uczestnik
    {
        [Key]
        public int Id { get; set; }
        [DisplayName("Imię i nazwisko")]
        public string Nazwisko { get; set; }
        [Required]
        [EmailAddress(ErrorMessage = "Zły adres e-mail")]
        public string Email { get; set; }
        public string Telefon { get; set; }
        public bool? CzyUczestniczy { get; set; }


    }
}